import sys
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

class SubWindow1(QWidget):
    def __init__(self):
        super(SubWindow1, self).__init__()
        self.resize(400, 300)

        # Label
        self.label = QLabel(self)
        self.label.setGeometry(0, 0, 400, 300)
        self.label.setText('Sub Window 1')
        self.label.setAlignment(Qt.AlignCenter)
        self.label.setStyleSheet('font-size:40px')

class SubWindow2(QWidget):
    def __init__(self):
        super(SubWindow2, self).__init__()
        self.resize(400, 300)

        # Label
        self.label = QLabel(self)
        self.label.setGeometry(0, 0, 400, 300)
        self.label.setText('Sub Window 2')
        self.label.setAlignment(Qt.AlignCenter)
        self.label.setStyleSheet('font-size:40px')