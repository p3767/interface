import sys
import os
import subprocess
import threading

from PyQt5.QtGui import QPixmap

from subwindow import *
from mainwindow import * 

from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

class MainApp(QMainWindow):
    def __init__(self):
        super(MainApp, self).__init__()
        self.setWindowTitle('Application')
        self.resize(400, 300)

        self.generalLayout = QVBoxLayout()
        self._centralWidget = QWidget(self)
        self.setCentralWidget(self._centralWidget)
        self._centralWidget.setLayout(self.generalLayout)

        self._createButtons()

    def _createButtons(self):
        self.sub_window1 = SubWindow1()
#        self.sub_window2 = MainWindow()

        dlgLayout = QVBoxLayout()

        logoLabel = QLabel(self)
        pixmap = QPixmap('pulse-monitor.png')
        logoLabel.setPixmap(pixmap)
    
        button_1 = QPushButton(self)
        button_1.setText('Botón 1')
        button_1.setStyleSheet('font-size:40px')
        button_1.clicked.connect(self.runbutton1)
        

        button_2 = QPushButton(self)
        button_2.setText('Botón 2')
        button_2.setStyleSheet('font-size:40px')
 #       button_2.clicked.connect(self.sub_window2.show)
        
        dlgLayout.addWidget(logoLabel)
        dlgLayout.addWidget(button_1)
        dlgLayout.addWidget(button_2)
        
        self.generalLayout.addLayout(dlgLayout)
    
    def runbutton1(self):
        subprocess.call([r'C:/Users/kmila/Desktop/PDI-Interface/helloworld.bat'])




if __name__ == '__main__':
    app = QApplication([])
    window = MainApp()
    window.show()
    sys.exit(app.exec_())